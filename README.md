# dist-2-similarity

A very simple function that maps distance measure into similarity measure.

- distance: 0.0 ~ 17.0 ~ infinity.
- similarity: 0.0 ~ 1.0
