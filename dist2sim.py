import numpy as np
import math

def dist2sim(x):
    return sigmoid(log_base_half(x))

def log_base_half(x):
    if (x <= 0): x = 0.000000001
    return math.log(x, 1/2)

def sigmoid(x):
    return 1/(1 + np.exp(-x))

nums = [0.0001, 0.1, 0.2, 0.6, 0.7, 1.0, 1.2, 2.45, 3.0, 30.0, 100.0]

print("x, sigmoid(x), log(x,1/2), sigmoid(log(x,1/2))")
for x in nums:
    print("%.3f, %.3f, %.3f, %.3f" % (x, sigmoid(x), log_base_half(x), dist2sim(x)))


#x, sigmoid(x), log(x,1/2), sigmoid(log(x,1/2))
# 0.000, 0.500, 13.288, 1.000
# 0.100, 0.525, 3.322, 0.965
# 0.200, 0.550, 2.322, 0.911
# 0.600, 0.646, 0.737, 0.676
# 0.700, 0.668, 0.515, 0.626
# 1.000, 0.731, -0.000, 0.500
# 1.200, 0.769, -0.263, 0.435
# 2.450, 0.921, -1.293, 0.215
# 3.000, 0.953, -1.585, 0.170
# 30.000, 1.000, -4.907, 0.007
# 100.000, 1.000, -6.644, 0.001